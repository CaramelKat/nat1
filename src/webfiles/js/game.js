/**
 * Connect from text box
 */
function con() {
    matchmaking.sendMessage({ type: 'update_request', id: matchmaking.id() });
    document.getElementById('messages').innerText += '=====You Joined the Server=====\n';
    matchmaking.sendMessage({ type: 'message', content: { text: '=====' + matchmaking.id() + ' Joined the Server=====' } });
}

/**
 * Sends a message from the text box
 * @param event
 */
function sendMessage(event) {
    if (event.keyCode === 13) {
        document.getElementById('messages').innerText += 'Me: ' + document.getElementById('chat').value + '\n';
        matchmaking.sendMessage({ type: 'message', content: { text: matchmaking.id() + ': ' + document.getElementById('chat').value } });
        document.getElementById('chat').value = '';
    }
}

/**
 * Process data from incoming message
 * @param data
 */
function processData(data) {
    console.log('Received ' + data.type)
    switch (data.type) {
        case 'message':
            document.getElementById('messages').innerText += data.content.text + '\n';
            break;
        case 'chip':
            makeChip(data);
            break;
        case 'update_request':
            updateData(data);
            break;
        default :
            setChip(data);
            break;
    }
}

/**
 * Generate new chip with random colors and ID
 */
function generateNewChip() {
    var randomColor = '#'+Math.floor(Math.random()*16777215).toString(16);
    var elementID = Math.random()
    var chip = '<div id="' + elementID + '" class="chip" style="background: ' + randomColor +'"></div>';
    document.getElementsByClassName('map-content')[0].innerHTML += chip;
    initChips();
    var element = document.getElementById(elementID);
    matchmaking.sendMessage({ type: 'chip', content: {element_id: elementID, color: randomColor, top: element.style.top, left: element.style.left} });
}

/**
 * Create chip from data send from other clients
 * @param data
 */
function makeChip(data) {
    document.getElementsByClassName('map-content')[0].innerHTML += '<div id="' + data.content.element_id + '" class="chip" style="background: ' + data.content.color +'; top: ' + data.content.top + '; left: ' + data.content.left + ';"></div>';
    dragElement(document.getElementById(data.content.element_id));
    initChips();
}

/**
 * Set chip position from other clients
 * @param data
 */
function setChip(data) {
    var element = document.getElementById(data.element_id);
    element.style.top = data.content.top;
    element.style.left = data.content.left;
}

/**
 * Add event listener back to each chip
 * after a DOM refresh
 */
function initChips() {
    var chips = document.getElementsByClassName('chip')
    for (var i = 0; i < chips.length; i++) {
        dragElement(chips[i]);
    }
}

/**
 * Move element based on mouse position
 * @param elmnt
 */
function dragElement(elmnt) {
    var pos1 = 0, pos2 = 0, pos3 = 0, pos4 = 0;
    elmnt.onmousedown = dragMouseDown;
    function dragMouseDown(e) {
        e = e || window.event;
        e.preventDefault();
        // get the mouse cursor position at startup:
        pos3 = e.clientX;
        pos4 = e.clientY;
        document.onmouseup = closeDragElement;
        // call a function whenever the cursor moves:
        document.onmousemove = elementDrag;
    }

    function elementDrag(e) {
        e = e || window.event;
        e.preventDefault();
        // calculate the new cursor position:
        pos1 = pos3 - e.clientX;
        pos2 = pos4 - e.clientY;
        pos3 = e.clientX;
        pos4 = e.clientY;
        // set the element's new position:
        elmnt.style.top = (elmnt.offsetTop - pos2) + "px";
        elmnt.style.left = (elmnt.offsetLeft - pos1) + "px";
        matchmaking.sendMessage({ type: 'movement', element_id: elmnt.id, content: {top: elmnt.style.top, left: elmnt.style.left} });
    }

    function closeDragElement() {
        // stop moving when mouse button is released:
        document.onmouseup = null;
        document.onmousemove = null;
    }
}

/**
 * Sends status of all current objects to new client
 * @param data
 */
function updateData(data) {
    console.log('New Connection')
    var chips = document.getElementsByClassName('chip')
    for (var i = 0; i < chips.length; i++) {
        matchmaking.sendMessageTo(data.id,{ type: 'chip', content: {element_id: chips[i].id, color: chips[i].style.background, top: chips[i].style.top, left: chips[i].style.left} });
    }
}

/**
 * Check if body contains pre-set ID to use instead of the random one
 */
if(document.body.getAttribute('data-my-id') !== null)
    var my_id = document.body.getAttribute('data-my-id');

/**
 * Allows map to be dragged around with the mouse
 */
document.addEventListener('DOMContentLoaded', function() {
    const ele = document.getElementById('map');
    ele.style.cursor = 'grab';

    let pos = { top: 0, left: 0, x: 0, y: 0 };

    const mouseDownHandler = function(e) {
        e = window.event || e;
        if(this.children[0] === e.target) {
            ele.style.cursor = 'grabbing';
            ele.style.userSelect = 'none';

            pos = {
                left: ele.scrollLeft,
                top: ele.scrollTop,
                // Get the current mouse position
                x: e.clientX,
                y: e.clientY,
            };

            document.addEventListener('mousemove', mouseMoveHandler);
            document.addEventListener('mouseup', mouseUpHandler);
        }
    };

    const mouseMoveHandler = function(e) {
        // How far the mouse has been moved
        const dx = e.clientX - pos.x;
        const dy = e.clientY - pos.y;

        // Scroll the element
        ele.scrollTop = pos.top - dy;
        ele.scrollLeft = pos.left - dx;
    };

    const mouseUpHandler = function() {
        ele.style.cursor = 'grab';
        ele.style.removeProperty('user-select');

        document.removeEventListener('mousemove', mouseMoveHandler);
        document.removeEventListener('mouseup', mouseUpHandler);
    };

    // Attach the handler
    ele.addEventListener('mousedown', mouseDownHandler);
});

/**
 * Create new instance of the Matchmaking class to handle all p2p connections
 * @type {Matchmaking}
 */
const matchmaking = new Matchmaking(processData, null, { id: my_id, args: {debug: 0} }, function () {
    document.getElementById('peer').innerText = matchmaking.id();
    if(document.body.getAttribute('data-game-id') !== null)
        matchmaking.newConnection(document.body.getAttribute('data-game-id'), con);
});
