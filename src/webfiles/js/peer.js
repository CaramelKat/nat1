/*
 * Simple wrapper class for PeerJS to maintain simultaneous connections
 * across more than one client, as well as a simple way to send data between them.
 */
class Matchmaking {
    /**
     *
     * @param processDataFunction
     * @param onNewConnectionFunction
     * @param options
     * @param callback
     */
    constructor(processDataFunction, onNewConnectionFunction, options, callback) {
        var my_id = null, peers = [], conn = null, peer = new Peer(options.id, options.args);

        /**
         * Get your own ID from the matchmaking server
         */
        peer.on('open', function(id) {
            my_id = id;
            callback();
        });

        /**
         * When new data is received, hand it off to the parseData function
         * If the data is from a new connection, add it to the peers list and
         * notify the other clients
         */
        peer.on('connection', function(con){
            con.on('data', data => parseData(data));
            if(peers.indexOf(con) === -1) {
                peers.push(con);
                updatePeerList(con.peer, false);
                if(onNewConnectionFunction !== null)
                    onNewConnectionFunction();
            }
        });

        /**
         * Notify the other clients when a peer as disconnected, and update
         * the peer list
         */
        peer.on('disconnected', function(con){
            peers.forEach(function (peer, index, array) {
                if(peer.peer === con.peer) {
                    array.splice(index, 1);
                    updatePeerList(con.peer, true);
                    if(onNewConnectionFunction !== null)
                        onNewConnectionFunction();
                }
            });
        });

        /**
         * Returns current peer ID
         * @returns {string}
         */
        this.id = function () {
            return my_id;
        }

        /**
         * Parse the incoming data from the connection event, and pass it on to
         * the appropriate functions
         * @param data
         */
        var parseData = function (data) {
            if(data.type === 'peer') {
                if(peers.indexOf(data.content.id === -1) && data.content.id !== my_id)
                    connect(data.content.id);
                else if(data.content.remove) {
                    peers.forEach(function (peer, index, array) {
                        if(peer.peer === data.content.id)
                            array.splice(index, 1);
                    })
                }
            }
            else if(processDataFunction !== null)
                processDataFunction(data);
        }

        /**
         * Pushes new peer ID to other clients
         * @param peerID
         * @param remove
         */
        var updatePeerList = function (peerID, remove) {
            peers.forEach( function (peer) {
                peer.send({ type: 'peer', content: {id: peerID, remove: remove} });
            });
        }

        /**
         * Connects to a new peer by ID
         * @param id
         * @param callback
         */
        var connect = function (id, callback) {
            if(id === my_id)
                return;
            for(var i = 0; i < peers.length; i++) {
                if(peers[i].peer === id)
                    return;
            }
            conn = peer.connect(id);
            if(peers.indexOf(conn) === -1)
                peers.push(conn);
            conn.on('open', function() {
                conn.on('data', data => parseData(data));
                if(callback !== undefined)
                    callback();
            });
        }

        /**
         * sends message to all connected peers
         * @param contents
         */
        this.sendMessage = function (contents) {
            peers.forEach( function (peer) {
                peer.send(contents);
            });
        }

        /**
         * sends message to user by ID
         * @param id
         * @param contents
         */
        this.sendMessageTo = function (id, contents) {
            peers.every( function (peer) {
                if(peer.peer === id) {
                    peer.send(contents);
                    return false;
                }
                else
                    return true;
            });
        }

        /**
         * establishes a new connection by ID
         * @param ID
         * @param callback
         */
        this.newConnection = function(ID, callback) {
            connect(ID, callback)
        }
    }
}