const express = require('express');
const logger = require('../../logger');
const routes = require('./routes');

const router = express.Router();

logger.info('[WEB] Creating routes')

// Setup routes
router.use('/', routes.HOME);

module.exports = router;
