var express = require('express');
const database = require('../../../database');
const logger = require('../../../logger');
const config = require('../../../config.json');
const request = require("request");
var path = require('path');
var moment = require('moment');
const snowflake = require('node-snowflake').Snowflake;
var multer  = require('multer');
var upload = multer({ dest: 'uploads/' });
var router = express.Router();
var cookieParser = require('cookie-parser');

router.use(cookieParser());

router.get('/css/:file', function (req, res) {
    res.sendFile('css/' + req.params.file, {root: path.join(__dirname, '../../../webfiles/')});
});

router.get('/js/:file', function (req, res) {
    res.sendFile('js/' + req.params.file, {root: path.join(__dirname, '../../../webfiles/')});
});

router.get('/', upload.none(), function (req, res) {

    database.connect().then(async e => {
        res.render('home.ejs', {
            gameID: null,
            ID: snowflake.nextId()
        });

    }).catch(error => {
        res.statusCode = 400;
        res.send({
            error: error
        });
    });

});

router.get('/game/:connectionID', upload.none(), function (req, res) {

    database.connect().then(async e => {
        res.render('home.ejs', {
            gameID: req.params.connectionID,
            ID: snowflake.nextId(),
        });

    }).catch(error => {
        res.statusCode = 400;
        res.send({
            error: error
        });
    });

});

module.exports = router;
